#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <windows.h>
#include <winioctl.h>

#ifndef FAT32STRUCTS_H
#define FAT32STRUCTS_H

typedef unsigned char BYTE;
typedef unsigned short WORD;

#pragma pack(push, 1)
//FAT32 Boot Record Information
typedef struct FAT_BOOTSECTOR32
{
    // Common fields.
    BYTE sJmpBoot[3];       //Jump Code + NOP
    char sOEMName[8];       //OEM Name (Probably MSWIN4.1) Microsoft recommends "MSWIN4.1"
//BIOS Parameter Block
    //dos 2.0 bpb
    WORD wBytsPerSec;       //Bytes Per Sector
    BYTE bSecPerClus;       //Sectors Per Cluster
    WORD wRsvdSecCnt;       //Reserved Sectors
    BYTE bNumFATs;          //Number of File Allocation Tables.
    WORD wRootNum;       //Количество элементов корневого каталога
    WORD wTotSec16;         // Всего логических секторов на диске if zero, use dTotSec32 instead Number of Sectors inPartition Smaller than 32MB (N/A for FAT32)
    BYTE bMedia;            //Media Descriptor (F8h forHard Disks) Тип носителя
    WORD wSecPerFat;          //Sectors Per FAT in Older FATSystems (N/A for FAT32) Логических секторов в FAT
    //DOS 3.31 ВРВ
    WORD wSecPerTrk;        //Sectors Per Track
    WORD wNumHeads;         //Number of Heads
    DWORD dHiddSec;         //Number of Hidden Sectors inPartition
    DWORD dTotalSec;        //Number of Sectors inPartition
    //DOS 7.1 EBPB
    DWORD dFATSz32;          //Размер FAT в логических секторах
    WORD wFlags;         //Flags (Bits 0-4 IndicateActive FAT Copy)
                            //(Bit 7 Indicates whether FAT Mirroringis Enabled or Disabled )
                            //(If FATMirroring is Disabled, the FAT Information is onlywritten to the copy indicated by bits 0-4)
    WORD wFSVer;            //Version of FAT32 Drive (HighByte = Major Version, Low Byte = Minor Version)
    DWORD dRootClus;        //Корневой каталог в кластерах Cluster Number of the Start of the Root Directory
    WORD wFSInfo;           // Расположение FSI-структуры Sector Number of the FileSystem Information Sector (See Structure Below)(Referenced from the Start of the Partition)
    WORD wBkBootSec;        //Расположение резервных секторов Sector Number of the BackupBoot Sector (Referenced from the Start of the Partition)
    BYTE Reserved[12];
    BYTE bDrvNum;           //Номер физического диска Logical Drive Number ofPartition
    BYTE Reserved1;         //Флаги Unused (Could be High Byteof Previous Entry)
    BYTE bBootSig;          // Расширенная загрузочная запись == 0x29 if next three fields are ok  Extended Signature (29h)
    DWORD dSerialNum;        //Serial number of partition
    char sVolLab[11];       //Volume label Метка тома
    BYTE sBS_FilSysType[8]; //Filesystem type ("FAT32   ")

} FAT_BOOTSECTOR32;
//File System Information Sector instruction
//FAT32 хранит дополнительную информацию в секторе FSInfo, обычно в секторе 1.
typedef struct {
    DWORD dLeadSig;         // 0x41615252  First Signature (52h 52h 61h41h)
    BYTE sReserved1[480];   // zeros   Unknown, Currently (Mightjust be Null)
    DWORD dStrucSig;        // 0x61417272   Signature of FSInfo Sector(72h 72h 41h 61h)
    DWORD dFree_Count;      // 0xFFFFFFFF   Number of Free Clusters (Setto -1 if Unknown)
    DWORD dNxt_Free;        // 0xFFFFFFFF   Cluster Number of Clusterthat was Most Recently Allocated.
    BYTE sReserved2[12];    // zeros  Unknown or Null
    DWORD dTrailSig;     // 0xAA550000   Boot Record Signature (55hAAh)
} FAT_FSINFO;


#pragma pack(pop)
#endif // FAT32STRUCTS_H
