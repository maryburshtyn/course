#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <QApplication>
#include <windows.h>
#include <winioctl.h>
#include "fat32structs.h"

#ifndef FAT32FORMAT_H
#define FAT32FORMAT_H


class FAT32Format
{
public:
     DWORD *pFat;
     FAT_BOOTSECTOR32 *pFAT32BootSect;
     FAT_FSINFO *pFAT32FsInfo;
     HANDLE hDevice;
     int sectors;
     QString path;
     DWORD SerialNumber;
     DWORD SectorsPerCluster;
     DWORD BytesPerSect;
     DWORD SystemAreaSize;
     DWORD FatSize;
     BOOL bGPTMode;
     DISK_GEOMETRY dgDrive;
     PARTITION_INFORMATION piDrive;
     PARTITION_INFORMATION_EX xpiDrive;
public:
     FAT32Format();
     ~FAT32Format(){}
     void setPath(QString path);
     void openDevice();
     void getSerialNumber();
     void getSectorsInCluster();
     void getPartitionInfo();
     void setFormatInfo(int Sectors);
     void getFATSize();
     void format();
     void error(QString error);
     void info(QString info);
     void moveHandle(DWORD Sector);
     void cleanMemory(DWORD NumSects, DWORD SectorNum);
     void writeSector(DWORD Sector,void *Data, DWORD NumSects );
};

#endif // FAT32FORMAT_H
