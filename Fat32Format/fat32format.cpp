#include "fat32format.h"
#include <QMessageBox>

FAT32Format::FAT32Format()
{
    this->hDevice = 0;
    this->sectors = 0;
    this->path = "";
    this->SerialNumber = 0;
    this->SectorsPerCluster = 0;
    this->BytesPerSect = 0;
    this->SystemAreaSize = 0;
    this->FatSize = 0;
    this->bGPTMode = false;
}
void FAT32Format::error(QString error){
    QMessageBox messageBox;
    messageBox.critical(0,"Error",error,QMessageBox::StandardButton::Ok);
    messageBox.setFixedSize(500,200);
    QApplication::quit();
}
void FAT32Format::info(QString info){
    QMessageBox messageBox;
    messageBox.information(0,"Information",info,QMessageBox::StandardButton::Ok);
    messageBox.setFixedSize(500,200);

}

void FAT32Format::moveHandle(DWORD Sector )
{
    LONGLONG Offset;
    LONG HiOffset;

    Offset = Sector * this->BytesPerSect ;
    HiOffset = (LONG) (Offset>>32);
    SetFilePointer ( this->hDevice, (LONG) Offset , &HiOffset , FILE_BEGIN );
}
void FAT32Format::writeSector( DWORD Sector, void *Data, DWORD NumSects )
{
    DWORD dwWritten;
    BOOL ret;

    moveHandle ( Sector );
    ret=WriteFile ( this->hDevice, Data, NumSects*this->BytesPerSect, &dwWritten, NULL );

    if ( !ret )
        error ( "Failed to write" );
}
void FAT32Format::cleanMemory(DWORD NumSects, DWORD SectorNum ){
    BYTE *pZeroSect;
    DWORD WriteSize;
    BOOL ret;
    DWORD dwWritten;


    pZeroSect = (BYTE*) VirtualAlloc( NULL, this->BytesPerSect*this->SectorsPerCluster, //размер кластера в байтах
    MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
    moveHandle( SectorNum);

    while (NumSects)
    {
        if (NumSects > this->SectorsPerCluster )
            WriteSize = this->SectorsPerCluster;
        else
            WriteSize = NumSects;

        ret=WriteFile ( this->hDevice,                //дескриптор файла
                        pZeroSect,              //Указатель на буфер, содержащий данные, которые будут записаны в файл
                        WriteSize*this->BytesPerSect, //Число байтов, которые будут записаны в файл.
                        &dwWritten,             //число записанных байтов
                        NULL );                 //не асинхронный процесс
        if ( !ret )
            error ( "Failed to write" );

        NumSects -= WriteSize;
    }
    info("cleanMemory succeed");
}

void FAT32Format::setPath(QString path){
    this->path = path;
}
void FAT32Format::getSerialNumber(){

    char VolumeNameBuffer[100];
    char FileSystemNameBuffer[100];
    DWORD sz,fs,drive_sn;

    if (! GetVolumeInformation(this->path.toStdWString().c_str(),(LPTSTR)VolumeNameBuffer, sizeof(VolumeNameBuffer),
                             &drive_sn,&sz,&fs,(LPTSTR)FileSystemNameBuffer,sizeof(FileSystemNameBuffer))){
        int i = GetLastError();
        QString error = "GetLastError() = " + QString::number(i);
    }
   this->SerialNumber =  drive_sn;
}
void FAT32Format::getSectorsInCluster(){
    DWORD dwSectPerClust, dwBytesPerSect, dwFreeClusters, dwTotalClusters;
    if(! GetDiskFreeSpace(path.toStdWString().c_str(), &dwSectPerClust, &dwBytesPerSect, &dwFreeClusters, &dwTotalClusters)) {
        int i = GetLastError();
        error("Failed to get current sectors in cluster"+ QString::number(i));
    }
    else this->SectorsPerCluster = (BYTE)dwSectPerClust;
}
void FAT32Format::openDevice(){
    QString path = this->path;
    this->getSectorsInCluster();
    this->getSerialNumber();
    int returned = 0;
    BOOL checkReturn;
    QString devicePath="\\\\.\\";
    devicePath+=this->path.at(0);
    devicePath+=this->path.at(1);
    HANDLE hDevice;
    hDevice = CreateFile (
           devicePath.toStdWString().c_str(),
           GENERIC_READ | GENERIC_WRITE,
           0 ,                     //Запрещает другим процессам открывать файл или устройство, если они запрашивают удаление, чтение или запись.
           NULL,
           OPEN_EXISTING,
           FILE_FLAG_NO_BUFFERING,     //Файл или устройство открывается без системного кэширования для чтения и записи данных. Этот флаг не влияет на кэширование жесткого диска или файлы с отображением памяти.
           NULL);

       if ( hDevice ==  INVALID_HANDLE_VALUE ){
           int i = GetLastError();
           error("Coudn't open device - close any files before formatting" + QString::number(i));
       }

       checkReturn= DeviceIoControl(
         (HANDLE) hDevice,
         //подает сигнал драйверу файловой системы, чтобы не выполнять любую проверку границ ввода - вывода
         //при вызове операций чтения или записи в раздел. Вместо этого, проверки границ выполняются драйвером устройства.
         FSCTL_ALLOW_EXTENDED_DASD_IO,  // dwIoControlCode
         NULL,                          // lpInBuffer
         0,                             // nInBufferSize
         NULL,                          // lpOutBuffer
         0,                             // nOutBufferSize
         (LPDWORD)returned,				 // number of bytes returned
         NULL                           // OVERLAPPED structure
       );

       if ( !checkReturn )    {
          error("Failed to allow extended DASD on device");
       }


       checkReturn = DeviceIoControl( hDevice, FSCTL_LOCK_VOLUME, NULL, 0, NULL, 0, (LPDWORD)returned, NULL );

       if ( !checkReturn ){
         error("Failed to lock device");
       }
       info("openDevice succeed!");

       this->hDevice = hDevice;
}
void FAT32Format::getPartitionInfo(){
    int returned = 0;
    BOOL bGPTMode = FALSE;
    ULONGLONG qTotalSectors=0;
    BOOL checkReturn;
    DISK_GEOMETRY diskGeometry;
    PARTITION_INFORMATION  partitionInfo;
    PARTITION_INFORMATION_EX xpiDrive;
    checkReturn = DeviceIoControl ( this->hDevice, IOCTL_DISK_GET_DRIVE_GEOMETRY,
            NULL, 0, &diskGeometry, //A pointer to a buffer that receives a DISK_GEOMETRY data structure.
            sizeof(diskGeometry),
            (LPDWORD)returned, NULL);
    if ( !checkReturn )
        error( "Failed to get device geometry" );
    this->dgDrive = diskGeometry;

    checkReturn = DeviceIoControl ( hDevice,
        IOCTL_DISK_GET_PARTITION_INFO,
        NULL, 0, &partitionInfo,      //A pointer to a buffer that receives a PARTITION_INFORMATION data structure.
         sizeof(partitionInfo),
        (LPDWORD)returned, NULL);
    if ( !checkReturn )
    {
        checkReturn = DeviceIoControl ( hDevice,
        IOCTL_DISK_GET_PARTITION_INFO_EX,
        NULL, 0, &xpiDrive, sizeof(xpiDrive),
        (LPDWORD)returned, NULL);

        if (!checkReturn)
            error( "Failed to get partition info" );

        memset ( &partitionInfo, 0, sizeof(partitionInfo) );
        partitionInfo.StartingOffset.QuadPart = xpiDrive.StartingOffset.QuadPart;
        partitionInfo.PartitionLength.QuadPart = xpiDrive.PartitionLength.QuadPart;
        partitionInfo.HiddenSectors = (DWORD) (xpiDrive.StartingOffset.QuadPart / diskGeometry.BytesPerSector);


        bGPTMode = ( xpiDrive.PartitionStyle == PARTITION_STYLE_MBR ) ? 0 : 1;
        this->bGPTMode =  bGPTMode;

    }
    this->piDrive = partitionInfo;
    this->BytesPerSect = diskGeometry.BytesPerSector;


    qTotalSectors = partitionInfo.PartitionLength.QuadPart/diskGeometry.BytesPerSector; //QuadPart - A signed 64-bit integer.
    if ( qTotalSectors < 65536 )
    {
        error ( "This drive is too small for FAT32 - there must be at least 64K clusters\n" );
    }

    if ( qTotalSectors >= 0xffffffff )
    {
        error ("This drive is too big for FAT32 - max 2TB supported\n" );
    }
     info("getPartitionInfo succeed");
}
void FAT32Format::getFATSize(){

    ULONGLONG   a, b;
    ULONGLONG   FatElementSize = 4;
    ULONGLONG   FatSz;
    a = FatElementSize * ( this->pFAT32BootSect->dTotalSec - this->pFAT32BootSect->wRsvdSecCnt );
    b = ( this->pFAT32BootSect->bSecPerClus * this->BytesPerSect ) + ( FatElementSize * 2 );
    FatSz = a / b;
    FatSz += 1;

this->FatSize =  (DWORD) FatSz;
}
void FAT32Format::setFormatInfo(int sectors){

    DWORD TotalSectors=0;
    DWORD UserSects=0;
    ULONGLONG FatNeeded, FreeClusters;

    DWORD ReservedSectCount = 32;           // FAT32 uses 32
    DWORD NumFATs = 2;                      //usially 2 copies
    DWORD BackupBootSect = 6;

    char VolumeLabel[12] = "NO NAME    ";

    //memory for BPB
    this->pFAT32BootSect = (FAT_BOOTSECTOR32*) VirtualAlloc ( NULL, //задает начальный адрес, NULL-тогда операционная система сама выберет этот адрес.
                      this->BytesPerSect, //размер памяти.
                      MEM_COMMIT | MEM_RESERVE, //Выполняется выделение страниц памяти
                      //для непосредственной работы с ними. Выделенные страницы заполняются нулями.
                      //Сохраняем и фиксируем страницы
                      PAGE_READWRITE );           //тип доступа: Чтение и запись.
    //memory for FSInfo
    this->pFAT32FsInfo = (FAT_FSINFO*) VirtualAlloc( NULL, this->BytesPerSect, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );

    this->pFat = (DWORD*) VirtualAlloc( NULL, this->BytesPerSect, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );

    if ( !pFAT32BootSect || !pFAT32FsInfo || !pFat )
        error ( "Failed to allocate memory" );

    // заполнить загрузочный сектор и fs info
    pFAT32BootSect->sJmpBoot[0]=0xEB;
    pFAT32BootSect->sJmpBoot[1]=0x58;
    pFAT32BootSect->sJmpBoot[2]=0x90;

    strcpy( pFAT32BootSect->sOEMName, "MSWIN4.1" );
    pFAT32BootSect->wBytsPerSec = (WORD) BytesPerSect;

    if (sectors)          //если указали сколько секторов в кластере, то записывем их
        this->SectorsPerCluster = sectors;
    pFAT32BootSect->bSecPerClus = (BYTE) this->SectorsPerCluster ;
    pFAT32BootSect->wRsvdSecCnt = (WORD) ReservedSectCount;     // FAT32 uses 32.
    pFAT32BootSect->bNumFATs = (BYTE) NumFATs;                  //usially 2 copies
    pFAT32BootSect->wRootNum = 0;                            //Количество записей в корневом каталоге 0 для FAT32
    pFAT32BootSect->wTotSec16 = 0;                              //Общее количество секторов в файловой системе
    pFAT32BootSect->bMedia = 0xF8;
    pFAT32BootSect->wSecPerFat = 0;                    //Количество секторов на FAT,  0 для FAT32.
    pFAT32BootSect->wSecPerTrk = (WORD) this->dgDrive.SectorsPerTrack;    //кол-во секторов на дорожке
    pFAT32BootSect->wNumHeads = (WORD) this->dgDrive.TracksPerCylinder;   //кол-во дорожек на цилиндре
    pFAT32BootSect->dHiddSec = (DWORD) this->piDrive.HiddenSectors;       //кол-во скрытых секторов
    TotalSectors = (DWORD)  (this->piDrive.PartitionLength.QuadPart/this->dgDrive.BytesPerSector);
    pFAT32BootSect->dTotalSec = TotalSectors;

    this->getFATSize();
    //Это поле определено только для среды FAT32 и не существует на
    //FAT12 и FAT16. Это поле представляет собой 32-битное число FAT32
    //сектора, занятые ОДНОМ FAT. BPB_FATSz16 должен быть 0.
    pFAT32BootSect->dFATSz32 = this->FatSize;
    pFAT32BootSect->wFlags = 0;
    pFAT32BootSect->wFSVer = 0;             //Filesystem version
    pFAT32BootSect->dRootClus = 2;          //First cluster of root directory (usually 2)
    pFAT32BootSect->wFSInfo = 1;            //Номер сектора файловой системы в зарезервированной области FAT32 (обычно 1)
    pFAT32BootSect->wBkBootSec = (WORD) BackupBootSect;
    pFAT32BootSect->bDrvNum = 0x80;         //Номер логического диска
    pFAT32BootSect->Reserved1 = 0;          //reserved
    pFAT32BootSect->bBootSig = 0x29;        //Указывает на наличие трех следующих полей.

    pFAT32BootSect->dSerialNum = this->SerialNumber;   //Serial number of partition
    memset( VolumeLabel, 0x20, 11 );
    VolumeLabel[0] = path.at(0).toLatin1();
    memcpy(this->pFAT32BootSect->sVolLab, VolumeLabel, 11 );
    memcpy(this->pFAT32BootSect->sBS_FilSysType, "FAT32   ", 8 );
    ((BYTE*)pFAT32BootSect)[510] = 0x55;
    ((BYTE*)pFAT32BootSect)[511] = 0xaa;

    //FAT32 хранит дополнительную информацию в секторе FSInfo, обычно в секторе 1.
    // FSInfo sect
    pFAT32FsInfo->dLeadSig = 0x41615252;        //the FSInfo signature
    pFAT32FsInfo->dStrucSig = 0x61417272;       // a second FSInfo signature
    pFAT32FsInfo->dFree_Count = (DWORD) - 1;     //
    pFAT32FsInfo->dNxt_Free = (DWORD) - 1;
    pFAT32FsInfo->dTrailSig = 0xaa550000;       //0xaa550000 - sector signature

    // First FAT pointer
    pFat[0] = 0x0ffffff8;  //самый первый указатель таблицы FAT содержит значение байта BPB_Media в его младших 8 битах, а все остальные биты имеют значение 1.
    pFat[1] = 0x0fffffff;  //Второй зарезервированный указатель, FAT[1], при форматировании устанавливается в значение метки EOC
    pFat[2] = 0x0fffffff;

    UserSects = TotalSectors - ReservedSectCount - (NumFATs*(this->FatSize));    //ко-во свободных секторов для пользователя(без Reserved region and FAt region)
    FreeClusters = UserSects/(this->SectorsPerCluster);                          //кол-во свободных кластеров
    if (  FreeClusters > 0x0FFFFFFF )
    {
        error ( "This drive has more than 2^28 clusters" );
    }
    if ( FreeClusters < 65536 )
    {
        error ( "FAT32 must have at least 65536 clusters"  );
    }
    FatNeeded = FreeClusters * 4;
    FatNeeded += (BytesPerSect - 1);
    FatNeeded /= BytesPerSect;
    if ( FatNeeded > FatSize )
    {
        error ( "This drive is too big for this version of fat32format" );
    }
    pFAT32FsInfo->dFree_Count = (UserSects/SectorsPerCluster)-1;
    pFAT32FsInfo->dNxt_Free = 3; // clusters 0-1 resered, we used cluster 2 for the root dir
    this->SystemAreaSize = (ReservedSectCount+(NumFATs*FatSize) + SectorsPerCluster);
    info("setFormatInfo succed");
}
void FAT32Format::format(){

    BOOL checkReturn;
    SET_PARTITION_INFORMATION spiDrive;
    int sizeRet = 0;
    cleanMemory(this->SystemAreaSize,0);
    for (int  i=0; i<2; i++ )
            {
            int SectorStart = (i==0) ? 0 : 6;
            writeSector ( SectorStart, pFAT32BootSect, 1 );
            writeSector (  SectorStart+1,pFAT32FsInfo, 1 );
            }
        for (int  i=0; i<2; i++ )
            {
            int SectorStart = 32 + (i * FatSize );
            writeSector (SectorStart, pFat, 1 );
            }
        if ( !(this->bGPTMode))
                {
                spiDrive.PartitionType = 0x0b;
                checkReturn = DeviceIoControl ( hDevice,
                    IOCTL_DISK_SET_PARTITION_INFO,  //Установка типа разделов диска
                    &spiDrive,                      //Указатель на буфер, содержащий данные раздела, которые необходимо установить.
                    sizeof(spiDrive),
                    NULL, 0,
                    (LPDWORD)sizeRet, NULL);

                if ( !checkReturn )
                    {
                    if ( this->piDrive.HiddenSectors  )
                        error( "Failed to set parition info" );
                    }
                }
        checkReturn = DeviceIoControl( hDevice, FSCTL_DISMOUNT_VOLUME, NULL, 0, NULL, 0, (LPDWORD)sizeRet, NULL );

            if ( !checkReturn )
                error( "Failed to dismount device" );


            checkReturn = DeviceIoControl( hDevice, FSCTL_UNLOCK_VOLUME, NULL, 0, NULL, 0, (LPDWORD)sizeRet, NULL );

            if ( !checkReturn )
                error( "Failed to unlock device" );

            // CloseDevice
            CloseHandle(this->hDevice);

            info("Device formatted");
}
