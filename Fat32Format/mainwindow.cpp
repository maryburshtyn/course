#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QApplication>
#include "fat32format.h"
#include "devices.cpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString str=avaliebleDevices();
    int i = 0;
    while(i!=str.size()){
        QString path;
        path += str.at(i).toLatin1();
        path+=":\\";
        QString type = getDiskType(path);
        ui->comboBox->addItem(path);
        i++;
    }
    ui->comboBox_2->addItem("use current");
    ui->comboBox_2->addItem("1");
    ui->comboBox_2->addItem("2");
    ui->comboBox_2->addItem("4");
    ui->comboBox_2->addItem("8");
    ui->comboBox_2->addItem("16");
    ui->comboBox_2->addItem("32");
    ui->comboBox_2->addItem("64");
    ui->comboBox_2->addItem("128");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page_2);

}

void MainWindow::on_CancelButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->page);
}

void MainWindow::on_pushButton_3_clicked()
{
    FAT32Format fat;

    int sect;
    QString sectors = ui->comboBox_2->currentText();
    if(sectors == "use current") sect = 0;
    else sect = sectors.toInt();

    QString path = ui->comboBox->currentText();
    fat.setPath(path);
    fat.openDevice();
    fat.getPartitionInfo();
    fat.setFormatInfo(sect);
    fat.format();
}

void MainWindow::on_comboBox_activated(int index)
{
   QString path = ui->comboBox->currentText();
   QString type = getDiskType(path);
   ui->label_6->setText(type);

}
