/********************************************************************************
** Form generated from reading UI file 'formatinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMATINFO_H
#define UI_FORMATINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_formatInfo
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;

    void setupUi(QDialog *formatInfo)
    {
        if (formatInfo->objectName().isEmpty())
            formatInfo->setObjectName(QStringLiteral("formatInfo"));
        formatInfo->resize(400, 300);
        horizontalLayoutWidget = new QWidget(formatInfo);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(170, 239, 195, 51));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(horizontalLayoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        horizontalLayout->addWidget(pushButton_2);

        pushButton = new QPushButton(horizontalLayoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        verticalLayoutWidget = new QWidget(formatInfo);
        verticalLayoutWidget->setObjectName(QStringLiteral("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(60, 40, 281, 161));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        label_2 = new QLabel(verticalLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout->addWidget(label_2);

        label_3 = new QLabel(verticalLayoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        verticalLayout->addWidget(label_3);

        label_4 = new QLabel(verticalLayoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        verticalLayout->addWidget(label_4);

        label_5 = new QLabel(verticalLayoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        verticalLayout->addWidget(label_5);


        retranslateUi(formatInfo);

        QMetaObject::connectSlotsByName(formatInfo);
    } // setupUi

    void retranslateUi(QDialog *formatInfo)
    {
        formatInfo->setWindowTitle(QApplication::translate("formatInfo", "Dialog", nullptr));
        pushButton_2->setText(QApplication::translate("formatInfo", "Ok", nullptr));
        pushButton->setText(QApplication::translate("formatInfo", "Cancel", nullptr));
        label->setText(QApplication::translate("formatInfo", "TextLabel", nullptr));
        label_2->setText(QApplication::translate("formatInfo", "TextLabel", nullptr));
        label_3->setText(QApplication::translate("formatInfo", "TextLabel", nullptr));
        label_4->setText(QApplication::translate("formatInfo", "TextLabel", nullptr));
        label_5->setText(QApplication::translate("formatInfo", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class formatInfo: public Ui_formatInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMATINFO_H
