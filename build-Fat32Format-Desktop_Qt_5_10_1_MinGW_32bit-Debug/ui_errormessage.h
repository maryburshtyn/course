/********************************************************************************
** Form generated from reading UI file 'errormessage.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ERRORMESSAGE_H
#define UI_ERRORMESSAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_ErrorMessage
{
public:

    void setupUi(QDialog *ErrorMessage)
    {
        if (ErrorMessage->objectName().isEmpty())
            ErrorMessage->setObjectName(QStringLiteral("ErrorMessage"));
        ErrorMessage->resize(413, 309);

        retranslateUi(ErrorMessage);

        QMetaObject::connectSlotsByName(ErrorMessage);
    } // setupUi

    void retranslateUi(QDialog *ErrorMessage)
    {
        ErrorMessage->setWindowTitle(QApplication::translate("ErrorMessage", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ErrorMessage: public Ui_ErrorMessage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ERRORMESSAGE_H
